#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <typeinfo>
#include <boost/algorithm/string.hpp>
#include "bondtradebookingservice.hpp"
#include "Bond_connectors.hpp"
#include "txt_read_write.hpp"
#include "bondinquiryservice.hpp"


using namespace std;

typedef vector<vector<string>> Matstrholder;


double BondPrice_to_double(const string & bondformat)
{
        vector<string> strs;
        boost::split(strs, bondformat, boost::is_any_of("-"));
        double integer_part = stod(strs[0]);
        string temp =string(1, strs[1].back());
        double remainder_part=stod(temp)/265.;
        double fractional_part=stod(strs[1].substr(0,2))*4./265.;

        return integer_part+fractional_part+remainder_part;

}

//for compartmental reason
vector<Bond> Bond_Creation()
{
    vector<Bond> Bond_vec;

    //creating 2 year treasury bond
    date maturityDate(2018, Nov, 11);
    string cusip2y = "912828U40";
    Bond yr2bond(cusip2y, CUSIP, "T", 0.01, maturityDate);
    Bond_vec.push_back(yr2bond);

   //creating 3 year treasury bond
    date maturityDate2(2019, Dec, 15);
    string cusip3y = "912828U73";
    Bond yr3bond(cusip3y, CUSIP, "T", 0.01375, maturityDate2);
    Bond_vec.push_back(yr3bond);

    //creating 5 year treasury bond
    date maturityDate3(2021, Nov, 30);
    string cusip5y = "912828U65";
    Bond yr5bond(cusip5y, CUSIP, "T", 0.01750, maturityDate3);
    Bond_vec.push_back(yr5bond);


    //creating 7 year treasury bond
    date maturityDate4(2021, Nov, 30);
    string cusip7y = "912828U57";
    Bond yr7bond(cusip7y, CUSIP, "T", 0.02215, maturityDate4);
    Bond_vec.push_back(yr7bond);


     //creating 10 year treasury bond
    date maturityDate5(2021, Nov, 30);
    string cusip10y = "912828U24";
    Bond yr10bond(cusip10y, CUSIP, "T", 0.02000, maturityDate5);
    Bond_vec.push_back(yr10bond);


     //creating 30 year treasury bond
    date maturityDate6(2021, Nov, 30);
    string cusip30y = "912810RU4";
    Bond yr30bond(cusip30y, CUSIP, "T", 0.02875, maturityDate6);
    Bond_vec.push_back(yr30bond);
}

void Trade_feeding(Matstrholder& trades, Connector<Trade<Bond>>& tradebooking_connector, vector<Bond> & Bondvec)
{
    map<string, int> switchers;
    switchers["TRSY2"] = 2;
    switchers["TRSY3"] = 3;
    switchers["TRSY5"] = 5;
    switchers["TRSY7"] = 7;
    switchers["TRSY10"] = 10;
    switchers["TRSY30"] = 30;

    map<string, Side> string2enum;

    string2enum["BUY"]=BUY;
    string2enum["SELL"]=SELL;

    for(auto row_iter:trades)
    {


        switch(switchers[row_iter[2]])
            {
              case 2: {
              Trade<Bond> temp_trade(Bondvec[0], row_iter[0], row_iter[2], stoi(row_iter[3]), string2enum[row_iter[4]]);
               tradebooking_connector.Publish(temp_trade);};

              case 3: {
              Trade<Bond> temp_trade(Bondvec[1], row_iter[0], row_iter[2], stoi(row_iter[3]), string2enum[row_iter[4]]);
               tradebooking_connector.Publish(temp_trade);};

              case 5: {
              Trade<Bond> temp_trade(Bondvec[2], row_iter[0], row_iter[2], stoi(row_iter[3]), string2enum[row_iter[4]]);
               tradebooking_connector.Publish(temp_trade);};

              case 7: {
              Trade<Bond> temp_trade(Bondvec[3], row_iter[0], row_iter[2], stoi(row_iter[3]), string2enum[row_iter[4]]);
               tradebooking_connector.Publish(temp_trade);};

              case 10: {
              Trade<Bond> temp_trade(Bondvec[4], row_iter[0], row_iter[2], stoi(row_iter[3]), string2enum[row_iter[4]]);
               tradebooking_connector.Publish(temp_trade);};

              case 30: {
              Trade<Bond> temp_trade(Bondvec[5], row_iter[0], row_iter[2], stoi(row_iter[3]), string2enum[row_iter[4]]);
               tradebooking_connector.Publish(temp_trade);};


            }


    }


}


void Price_feeding(Matstrholder &Price_holder, Connector<Price<Bond> >& Pricing_connector,  vector<Bond> &Bond_vec)
{
    for(auto row_iter = Price_holder.begin()+1; row_iter!=Price_holder.end(); row_iter++)
    {
        int bond_vec_idx; //to help with looping

        for(auto col_iter =(*row_iter).begin()+1:col_iter!=(*row_iter).end(); col_iter++)
        {
            double Price = BondPrice_to_double(*col_iter);

            Price<Bond> temp_price(Bond_vec[bond_vec_idx], Price, 1./128);

            Pricing_connector.Publish(temp_price);

            bon_vec_idx++;
        }

    }



}


void MktData_feeding(Matstrholder &MktData_holder, Connector<OrderBook<Bond> >& OrderBook_connector,  vector<Bond> &Bond_vec)
{



for(auto row_iter = Price_holder.begin()+1; row_iter!=Price_holder.end(); row_iter++)
    {
        int bond_vec_idx; //to help with looping

        for(auto col_iter =(*row_iter).begin()+1:col_iter!=(*row_iter).end(); col_iter++)
        {
            double midprice = BondPrice_to_double(*col_iter);

            vector<Order<Bond>> temp_bidstack, temp_offerstack;

            double spread = 1./256.;

            for(auto i=1; i!=5; i++)
            {
                Order<Bond> tmp_bid(midprice-i*spread, i*10000000, BID);
                Order<Bond> tmp_offer(midprice+i*spread, i*10000000, OFFER);

                temp_bidstack.push_back(tmp_bid);
                temp_offerstack.push_back(tmp_offer);

            }


            OrderBook<Bond> temp_orderbook(Bond_vec[bond_vec_idx], temp_bidstack, temp_offerstack);

            Orderbook_connector.Publish(temp_orderbook);

            bon_vec_idx++;
        }

    }

}


void Inquiries_feed(Matstrholder &Inquiry_holder, Connector<string, Inquiry<Bond>> &Inquiry_connector, vector<Bond> &Bond_vec)

{



        map<string, int> switchers;
    switchers["TRSY2"] = 2;
    switchers["TRSY3"] = 3;
    switchers["TRSY5"] = 5;
    switchers["TRSY7"] = 7;
    switchers["TRSY10"] = 10;
    switchers["TRSY30"] = 30;

    map<string, Side> string2enum;

    string2enum["BUY"]=BUY;
    string2enum["SELL"]=SELL;

    for(auto row_iter:Inquiry_holder)
    {


        switch(switchers[row_iter[2]])
            {
              case 2: {
              Inquiry<Bond> temp_inquiry(row_iter[0], Bond_vec[0], string2enum(row_iter[4]), stoi(row_iter[3]),stod(row_iter[4]), RECEIVED);
               inquiry_connector.Publish(temp_trade);};

              case 3: {
              Inquiry<Bond> temp_inquiry(row_iter[0], Bond_vec[1], string2enum(row_iter[4]), stoi(row_iter[3]),stod(row_iter[4]), RECEIVED);
               inquiry_connector.Publish(temp_trade);};

              case 5: {
              Inquiry<Bond> temp_inquiry(row_iter[0], Bond_vec[2], string2enum(row_iter[4]), stoi(row_iter[3]),stod(row_iter[4]), RECEIVED);
               inquiry_connector.Publish(temp_trade);};

              case 7: {
              Inquiry<Bond> temp_inquiry(row_iter[0], Bond_vec[3], string2enum(row_iter[4]), stoi(row_iter[3]),stod(row_iter[4]), RECEIVED);
               inquiry_connector.Publish(temp_trade);};

              case 10: {
              Inquiry<Bond> temp_inquiry(row_iter[0], Bond_vec[4], string2enum(row_iter[4]), stoi(row_iter[3]),stod(row_iter[4]), RECEIVED);
               inquiry_connector.Publish(temp_trade);};

              case 30: {
              Inquiry<Bond> temp_inquiry(row_iter[0], Bond_vec[5], string2enum(row_iter[4]), stoi(row_iter[3]),stod(row_iter[4]), RECEIVED);
               inquiry_connector.Publish(temp_trade);};


            }


    }


}

int main(int argc, char* argv)
{
    cout<<"Loading data"<<endl;

    vector<Bond> Bond_vec = Bond_creation();

    Matstrholder Trade_holder, Price_holder, Mktdata_holder, Inquiry_holder;

    Trade_holder = finput("trades.txt");

    Price_holder = finput("prices.txt");

    Mktdata_holder = finput("marketdata.txt");

    Inquiry_holder = finput("inquiries.txt");


    cout<<"Feeding data through services"<<endl;

    //initializing bond objects


    //initialize connector and service objects
    BondTradeBookingService<Bond> BTbService;

    //initialize BTBS_connector and getting the address of the BTBS object
    BondTradeBookingService_Connector<Bond, Trade<T>> BTBS_connector(&BTbService);

    //feeding trades forward
    Trade_feeding(Trade_holder, BTBS_connector, Bond_vec);

    //initialize BPS service
    BondPricingService<Bond> BPService;
    //initialize listener
    BondServiceListener<Bond> BTBS2BPS_listener(&BPService);

    BTbService.AddListener(&BTBS2BPS_listener);

    auto tradebookdata = BTbService.GetData("TRSY2");

    BTBS2BPS_listener.ProcessAdd(tradebookdata);

    //initialize
    BondHistoricalDataService<Bond> BHDService;
    BondServiceListener<Bond> BPS2HDS_listener(&BHDService);

    auto BPSdata =BPService.GetData("TRSY2);
    BPS2HDS_listener.ProcessAdd(BPSdata);


    /**********************************************************************/
    BondMarketDataService<Bond> BMDService;
    BondMarketDataService_Connector<Bond> BMDS_connector(&BMDService);

    MktData_feeding(Mktdata_holder, BMDS_connector, Bond_vec)

    BondAlgoExecutionService<Bond> BAEService;
    BMDS2BAES_listener<Bond> BMDS2BAES_listener1(&BAEService);


    BMDService.AddListener(&BMDS2BAES_listener1);

    auto BMDSdata = BMDService.GetData("TRSY3");

    BMDS2BAES_listener.ProcessAdd(BMDSdata);





    return 0;
}
