#ifndef BONDALGOSTREAMINGSERVICE_HPP_INCLUDED
#define BONDALGOSTREAMINGSERVICE_HPP_INCLUDED

#include "bondmarketdataservice.hpp"
#include "streamingservice.hpp"

template<typename T>
class BondAlgoStreamingService: public StreamingService<T>
{
public:

    BondAlgoStreamingService();

  // Get data on our service given a key
   PriceStream<T>& GetData(string key);

  // Decision Fucntion
   void StreamAlgo(OrderBook<T> &data);

  // Add a listener to the Service for callbacks on add, remove, and update events
  // for data to the Service.
   void AddListener(ServiceListener<PriceStream<T>> *listener) = 0;

  // Get all listeners on the Service.
   const vector< ServiceListener<PriceStream<T>>* >& GetListeners() const = 0;

private:

   vector< ServiceListener<PriceStream<T>>* > BASS_listener_vec;


    map<string, PriceStream<T>>  AlgoPriceStreamRecord;

    long OrderId_counter = 0; //This will be used as OrderId as it gets added up
};


template<typename T>
BondAlgoStreamService<T>::BondAlgoStreamService()
{

    AlgoPriceStreamRecord = map<string, PriceStream<T>>();
    BASS_listener_vec = vector< ServiceListener<PriceStream<T>>* > ();

}

template<typename T>
PriceStream<T> & BondAlgoStreamingService<T>::GetData(string key)
{

    return AlgoPriceStreamRecord[key];

}


template<typename T>
void BondAlgoStreamingService::StreamAlgo(OrderBook<T> &data)
{

//streaming the bid order
    auto bid_begin_ptr = data.GetBidStack().begin();
    auto bid_end_ptr = data.GetBidStack().end();

    auto highestbid = *std::max_element(bid_begin_ptr, bid_end_ptr,Price_comp(bid_begin_ptr->GetPrice(), bid_end_ptr->GetPrice());

    long bid_hiddenquantity=0;

    for(bid_begin_ptr;bid_begin_ptr!=bid_end_ptr+1; bid_begin_ptr++)
        {
            bid_hiddenquantity+=bid_being_ptr->GetQuantity();
        };

    PriceStreamOrder bidorder(highestbid.GetPrice(),Highestbid.GetQuantity(), bid_hiddenquantity, highestbid.GetSide()); )



//streaming the offer order
    auto offer_begin_ptr = data.GetOfferStack().begin();
    auto offer_end_ptr = data.GetOfferStack().end();

    auto lowestoffer = *std::min_element(offer_begin_ptr, offer_end_ptr,Price_comp(offer_begin_ptr->GetPrice(), offer_end_ptr->GetPrice());

    long offer_hiddenquantity=0;

    for(offer_begin_ptr;offerd_begin_ptr!=offer_end_ptr+1; offer_begin_ptr++)
        {
            offer_hiddenquantity+=offer_being_ptr->GetQuantity();
        };

    //Market aggressing strategy
    PriceStreamOrder offerorder(lowestoffer.GetPrice(),lowestoffer.GetQuantity(), offer_hiddenquantity, lowestoffer.GetSide()); )


//package orders
    PriceStreamOrder<T>  tmp_streamorder(data.GetProduct(), bidorder, offerorder)
    AlgoPriceStreamRecord.insert(pair<string, PriceStreamOrder<T>>(data.GetProduct().GetProductId(), tmp_streamorder));

    OrderId_counter++;


}

#endif // BONDALGOSTREAMINGSERVICE_HPP_INCLUDED
