/**
    This file contains all derived T services implementations from the given base classes.

**/


#ifndef BONDTRADEBOOKINGERVICE_HPP_INCLUDED
#define BONDTRADEBOOKINGERVICE_HPP_INCLUDED


#include <string>
#include <vector>
#include <map>

#include "products.hpp"
#include "tradebookingservice.hpp"

#include "positionservice.hpp"


template<typename T>
class BondTradeBookingService : public TradeBookingService<Trade<T>>
{

public:
    //ctor
    BondTradeBookingService();

    Trade<T>& GetData(string key);

    //T would be a vector data coming from connector
    void OnMessage(Trade<T> &data);

    void AddListener(ServiceListener<Trade<T>> *listener);

    const vector< ServiceListener<Trade<T>>* >& GetListeners() const;

    void BookTrade( const Trade<T> & trade);

private:

    map<string, Trade<T> > TradingBook; //This is the main ledger

    //keeps list of all Servicelistener registered to this service
    vector< ServiceListener<Trade<T>>* > BondServiceListener_vec;
};



/* Implementation */

template<typename T>
BondTradeBookingService<T>::BondTradeBookingService()
{
    //initialize
    TradingBook = map< string, Trade<T> >() ;
    BondServiceListener_vec=vector< ServiceListener<Trade<T>>* >();
}


template<typename T>
Trade<T>&  BondTradeBookingService<T>::GetData(string productId)
{
    //please note this function returns a vector<<trade<T>>
    return TradingBook[productId];


}




template<typename T>
void BondTradeBookingService<T>::OnMessage(Trade<T>& data)
{
    //addend data to vector mapped to the same product ID, if not found, create new pair
    TradingBook[data.GetProduct().GetProductId()].push_back(data);

}

template<typename T>
void BondTradeBookingService<T>::AddListener(ServiceListener<Trade<T>> *listener)
{

    BondServiceListener_vec.push_back(listener);

}

template<typename T>
const vector< ServiceListener<Trade<T>>* >& BondTradeBookingService<T>::GetListeners() const
{
    return BondServiceListener_vec;

}

template<typename T>
void BondTradeBookingService<T>::BookTrade( const Trade<T> & trade)
{

    TradingBook.insert(pair< string, Trade<T> >(trade.GetProduct().GetProductId(), trade));


}
#endif // BONDTRADEBOOKINGERVICE_HPP_INCLUDED
