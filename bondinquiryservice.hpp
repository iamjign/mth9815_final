#ifndef BONDINQUIRYSERVICE_HPP_INCLUDED
#define BONDINQUIRYSERVICE_HPP_INCLUDED

#include <map>
#include "inquiryservice.hpp"

template<typename T>
class BondInquiryService : public InquiryService<Inquiry <T> >
{

public:

  BondInquiryService(Connector<Inquiry<T>> * _InquiryConnector);
  // Get data on our service given a key
  virtual Inquiry<T>& GetData(K key);

  // The callback that a Connector should invoke for any new or updated data
  virtual void OnMessage(Inquiry<T> &data);

  // Add a listener to the Service for callbacks on add, remove, and update events
  // for data to the Service.
  virtual void AddListener(ServiceListener<Inquiry<T>> *listener);

  // Get all listeners on the Service.
  virtual const vector< ServiceListener<Inquiry<T>>* >& GetListeners() const;
  // Send a quote back to the client
  void SendQuote(const string &inquiryId, double price);

  // Reject an inquiry from the client
  void RejectInquiry(const string &inquiryId);


private:

    vector< ServiceListener<Inquiry<T>>* > Inquirylistener_vec;

    multimap<string, Inquiry<T>> Inquirybook;

    //Just need one connector pointer to the circular logic
    Connector<Inquiry<T>> * InquiryConnector;

};


template<typename T>
BondInquiryService<T>::BondInquiryService(Connector<Inquiry<T>> * _InquiryConnector): InquiryConnector(_InquiryConnector)
{

    Inquirylistener_vec =vector< ServiceListener<Inquiry<T>>* >();

    Inquirybook = multimap<string, Inquiry<T>>();
}


template<typename T>
void BondInquiryService<T>::OnMessage(Inquiry<T>& data)
{


    Inquirybook.insert(pair<string, Inquiry<T>>(data.GetInquiryId(), data));

}

template<typename T>
void BondInquiryService<T>::Addlistener(erviceListener<Inquiry<T>> *listener)
{
    Inquirylistener_vec.push_back(listener);

}

template<typename T>
const vector< ServiceListener<Inquiry<T>>* >& BondInquiryService<T>::GetListeners() const
{
    return Inquirylistener_vec;

}


template<typename T>
void BondInquiryService<T>::SendQuote(const string &inquiryId, double price);
{
    Inquiry<T> temp_Inquiry(inquiryId, )
    InquiryConnector->Publish(Inquiry<T>& )
}

#endif // BONDINQUIRYSERVICE_HPP_INCLUDED
