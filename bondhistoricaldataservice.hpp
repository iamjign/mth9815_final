#ifndef BONDHISTORICALDATASERVICE_HPP_INCLUDED
#define BONDHISTORICALDATASERVICE_HPP_INCLUDED


#include "historicaldataservice.hpp"
#include "txt_read_write.hpp"


template<typename T>
class BondHistoricalDataService : HistoricalDataService<T>
{

public

    BondHistoricalDataService(Connector<T> * Connector);

    // Get data on our service given a key
    T& GetData(string key);

    // The callback that a Connector should invoke for any new or updated data
    void OnMessage(T &data);

    // Add a listener to the Service for callbacks on add, remove, and update events
    // for data to the Service.
    void AddListener(ServiceListener<T> *listener);

    // Get all listeners on the Service.
    const vector< ServiceListener<T>* >& GetListeners() const;
    // Persist data to a store
    void PersistData(string persistKey, const T& data);

private:
    vector< ServiceListener<T>* > BHDS_listener_vec;

    map<string, T> historical_record;

    Connector<T> * Connector;

};

template<typename T>
BondHistoricalDataService::BondHistoricalDataService(Connector<T> * _Connector): connector(_connector)
{
    BHDS_listener_vec=vector< ServiceListener<T>* >();
    historical_record = map<string, T>();

}

template<typename T>
T& BondHistoricalDataService::GetData(stringf key)
{

    BHDS_listener_vec[key];
}

template<typename T>
void BondHistoricalDataService::OnMessage(T &data)
{

    historical_record.insert(pair<string, T>()data.GetProduct().GetProductId(), data)
}

template<typename T>
void BondHistoricalDataService::AddListener(ServiceListener<T> *listener)
{

    BHDS_listener_vec.push_back(listener);
}

template<typename T>
const vector< ServiceListener<T>* >& BondHistoricalDataService::GetListeners() const
{
    return BHDS_listener_vec;

}

template<typename T>
void BondHistoricalDataService::PersistData(string persistKey, const T& data)
{

    connector->publish(persistKey, data);
}




#endif // BONDHISTORICALDATASERVICE_HPP_INCLUDED
