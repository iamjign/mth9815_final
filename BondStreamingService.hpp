#ifndef BONDSTREAMINGSERVICE_HPP_INCLUDED
#define BONDSTREAMINGSERVICE_HPP_INCLUDED

template<typename T>
class BondStreamingService : public Service<string,PriceStream <T> >
{

public:

    BondStreamingService();

    PriceStream<T> & GetData(string key);

    //
    void OnMessage(PriceStream<T> &data);

    // Add a listener to the Service for callbacks on add, remove, and update events
    // for data to the Service.
    void AddListener(ServiceListener<PriceStream<T>> *listener);

    // Get all listeners on the Service.
    const vector< ServiceListener<PriceStream<T>>* >& GetListeners() const;

        // Execute an order on a market
  void ExecuteOrder(const PriceStream<T>& order, Market market);

private:

    vector< ServiceListener<PriceStream<T>>* > BES_listener_vec;


    map<string, PriceStream<T>>  PriceStreamRecord;

    Connector<PriceStream<T>> *BSS2Market_connector;



};

template<typename T>
BondStreamingService::BondStreamingService(Connector<PriceStream<T>> *_BSS2Market_connector):
BES2Market_connector(_BSS2Market_connector)
{
    BSS_listener_vec=vector< ServiceListener<PriceStream<T>>* >();
    PriceStreamRecord=map<string, PriceStream<T>>();
}

template<typename T>
PriceStream<T> & BondStreamingService::GetData(string key)
{

    return PriceStreamRecord[key];
}

template<typename T>
void BondStreamingService::OnMessage(PriceStream<T> &data)
{
    ExcutionOrderRecord.insert(pair<string, PriceStream<T>>(data.GetProduct().GetProductId(), data));
}

template<typename T>
void BondStreamingService::AddListener(ServiceListener<PriceStream<T>> *listener)
{
    BES_listener_vec.push_bacK(listener);
}

template<typename T>
const vector< ServiceListener<PriceStream<T>>* >& BondStreamingService::GetListeners() const
{
    return BES_listener_vec;
}

template<typename T>
void BondStreamingService::ExecuteOrder(const PriceStream<T>& order, Market market)
{
    for(auto &iter:PriceStreamRecord)
    {
        BES2Market_connector->Publish(iter.second);

    }

}


#endif // BONDSTREAMINGSERVICE_HPP_INCLUDED
