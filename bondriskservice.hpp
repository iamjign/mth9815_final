#ifndef BONDRISKSERVICE_HPP_INCLUDED
#define BONDRISKSERVICE_HPP_INCLUDED

#include "riskservice.hpp"

template<typename T>
class BondRiskService: public RiskService<T>
{
public:

    PV01<T> &GetData(string key);

    void AddListener(ServiceListener<V> *listener);

    void AddPosition(Position<T> &position);

    const vector< ServiceListener<V>* >& GetListeners() const;

    const PV01<T>& GetBucketedRisk(const BucketedSector<T> &sector) const;

private:

    vector<ServiceListener<V>* > listener_vec;


};




#endif // BONDRISKSERVICE_HPP_INCLUDED
