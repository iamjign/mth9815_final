#ifndef TXT_READ_WRITE_HPP_INCLUDED
#define TXT_READ_WRITE_HPP_INCLUDED

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

template<typename _Matrix_type>
_Matrix_type finput(char* pathandname)
{
	typedef vector<string> vec_line;
	vector<vec_line> Matstrholder;
	string input_line;
	ifstream file(pathandname);
	if(!file.is_open())
		cerr<<"cannot open file!"<<endl;
	while(getline(file, input_line)) //obtaining line from ifstream object
	{
		vec_line ln;
		istringstream sline(input_line); //converting string back to stream for second loop extraction
		while(getline(sline, input_line, ','))//reusing input line to further break down data points
		{
			ln.push_back(input_line); //storing individual data point in the row vector<string>
		}
		Matstrholder.push_back(ln); //storing row vector chronological form
	}
	file.clear();
	file.close();//tidy up the file operations

	return Matstrholder;

}




#endif // TXT_READ_WRITE_HPP_INCLUDED
