#ifndef BONDALGOEXECUTIONSERVICE_HPP_INCLUDED
#define BONDALGOEXECUTIONSERVICE_HPP_INCLUDED

#include "bondmarketdataservice.hpp"
#include "executionservice.hpp"
#include <map>

template<typename T>
class BondAlgoExecutionService : public ExecutionService<T>
{

    BondAlgoExecutionService();

    ExecutionOrder<T> & GetData(string key);

    //Decision function
    void ExecuteAlgo(Orderbook<T> &data);

    // Add a listener to the Service for callbacks on add, remove, and update events
    // for data to the Service.
    void AddListener(ServiceListener<ExecutionOrder<T>> *listener);

    // Get all listeners on the Service.
    const vector< ServiceListener<ExecutionOrder<T>>* >& GetListeners() const;


private:

    vector< ServiceListener<ExecutionOrder<T>>* > BAES_listener_vec;


    map<string, ExecutionOrder<T>>  AlgoExecutionOrderRecord;

    long OrderId_counter = 0; //This will be used as OrderId as it gets added up
};


template<typename T>
BondExecutionService<T>::BondExecutionService()
{

    AlgoExecutionOrderRecord = map<string, ExecutionOrder<T>>();
    BAES_listener_vec = vector< ServiceListener<ExecutionOrder<T>>* > ();

}

template<typename T>
ExecutionOrder<T> & BondAlgoExecutionService<T>::GetData(string key)
{

    return AlgoExecutionOrderRecord[key];

}

template<typename T>
void BondAlgoExecutionService<T>::ExecuteAlgo(OrderBook<T> &data)
{

//aggress the bid order
    auto bid_begin_ptr = data.GetBidStack().begin();
    auto bid_end_ptr = data.GetBidStack().end();

    auto highestbid = *std::max_element(bid_begin_ptr, bid_end_ptr,Price_comp(bid_begin_ptr->GetPrice(), bid_end_ptr->GetPrice());

    long bid_hiddenquantity=0;

    for(bid_begin_ptr;bid_begin_ptr!=bid_end_ptr+1; bid_begin_ptr++)
        {
            bid_hiddenquantity+=bid_being_ptr->GetQuantity();
        };

    //Market aggressing strategy
    ExecutionOrder<T>  AggressingBidOrder(data.GetProduct(), OFFER, std::to_string(OrderId_counter), MARKET,  highestbid.GetPrice(), highestbid.GetQuantity(),bid_hidden_quantity,"NOPARENTORDER", False);

    AlgoExecutionOrderRecord.insert(pair<string, ExecutionOrder<T>>(highestbid.GetProduct().GetProductId(), AgressingBidOrder));

    OrderId_counter++;

//aggress the offer order
    auto offer_begin_ptr = data.GetOfferStack().begin();
    auto offer_end_ptr = data.GetOfferStack().end();

    auto lowestoffer = *std::min_element(offer_begin_ptr, offer_end_ptr,Price_comp(offer_begin_ptr->GetPrice(), offer_end_ptr->GetPrice());

    long offer_hiddenquantity=0;

    for(offer_begin_ptr;offerd_begin_ptr!=offer_end_ptr+1; offer_begin_ptr++)
        {
            offer_hiddenquantity+=offer_being_ptr->GetQuantity();
        };

    //Market aggressing strategy
    ExecutionOrder<T>  AggressingOfferOrder(data.GetProduct(), BID, std::to_string(OrderId_counter), MARKET,  lowestbid.GetPrice(), lowestbid.GetQuantity(),Offer_hidden_quantity,"NOPARENTORDER", False);

    AlgoExecutionOrderRecord.insert(pair<string, ExecutionOrder<T>>(lowestbid.GetProduct().GetProductId(), AgressingBidOrder));

    OrderId_counter++;
}


template<typename T>
void BondAlgoExecutionService<T>::AddListener(ServiceListener<ExecutionOrder<T>> *listener)
{
    BAES_listener_vec.push_back(listener);

}


template<typename T>
const vector< ServiceListener<ExecutionOrder<T>>* >& BondAlgoExecutionService<T>::GetListeners() const
{

    return BAES_listener_vec;
}







#endif // BONDALGOEXECUTIONSERVICE_HPP_INCLUDED
