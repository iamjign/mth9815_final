#ifndef BOND_CONNECTORS_HPP_INCLUDED
#define BOND_CONNECTORS_HPP_INCLUDED

#include "soa.hpp"
#include "bondtradebookingservice.hpp"
#include "bondalgoexecutionservice.hpp"
#include "bondmarketdataservice.hpp"
#include "bondinquiryservice.hpp"
#include "bondpositionservice.hpp"
#include "bondpricingservice.hpp"

#include <vector>
#include <algorithm>

template<typename T>
class BondTradeBookingService_Connector: public Connector<T>
{
public:

    //ctor
    BondTradeBookingService_Connector(BondTradeBookingService<T>* _Bookingservice_ptr); //passing TBTS object pointer

    //publish data to services
    void Publish(T &data);

private:
    BondTradeBookingService<T>* Bookingservice_ptr;
};


template<typename T>
class BondMarketDataService_Connector: public Connector<T>
{
public:

    //ctor
    BondTradeBookingService_Connector(BondTradeBookingService<T>* _historicalservice_ptr):historicalservice_ptr(_historicalservice_ptr){}; //passing TBTS object pointer

    //publish data to services
    void Publish(T &data){historicalservice_ptr->OnMessage(data);};

private:
    BondHistoricalDataService<T>* historicalservice_ptr;
};


template<typename T>
class BondTradePricingService_Connector: public Connector<T>
{
public:
    BondTradePricingService_Connector(BondTradePricingService<T>* _BTPS_ptr) BTPS_ptr(_BTPS_ptr);

    void Publish(T &data){BTPS_ptr->OnMessage(data);};

private:
    BondTradePricingService<T>* BTPS_ptr;

};

template<typename T>
class InquiryService_Connector: public Connector<T>
{
public:
    InquiryService_Connector(BondInquiryService<T>* _connector_ptr) connector_ptr(_connector_ptr);

    void Publish(T &data){BTPS_ptr->OnMessage(data);};

private:
    BondInquiryService<T>* connector_ptr;

};



template<typename T, typename ObjectType>
class BondServiceListener : public ServiceListener<T>
{


public:

    //ctor; this is to call the next service on a particular operation logic
    BondServiceListener(Service<string, ObjectType>* _Next_Service_ptr);

    //Modifying the data to next service
    void ProcessAdd(T &data);

   // void ProcessRemove(T &data);

    void ProcessUpdate(T &data);

private:
    Service<string, ObjectType>* Next_Service_ptr;

};
template<typename T>
class BMDS2BAES_listener : ServiceListener<OrderBook<T>>
{

public:

    BMSD2BAES_listener(Service<string, ExecutionOrder<T>> * _Next_Service_ptr);
    // Listener callback to process an add event to the Service
    void ProcessAdd(OrderBook<T> &data);

    // Listener callback to process a remove event to the Service
    //void ProcessRemove(OrderBook<T> &data);

    // Listener callback to process an update event to the Service
    //void ProcessUpdate(OrderBook<T> &data);

private:
    Service<string, ExecutionOrder<T>> *Next_Service_ptr;

}





/*implementation*/

template<typename T>
BondTradeBookingService_Connector<T>::BondTradeBookingService_Connector(BondTradeBookingService<T>* _Bookingservice_ptr): Bookingservice_ptr(_Bookingservice_ptr)
{
}

template<typename T>
void BondTradeBookingService_Connector<T>::Publish(T &data)
{

    Bookingservice_ptr->OnMessage(data);

}


template<typename T, typename ObjectType>
BondServiceListener<T, ObjectType>::BondServiceListener(Service<string, ObjectType> * _Next_Service_ptr) : Next_Service_ptr(_Next_Service_ptr)
{

}

template<typename T, typename ObjectType>
void BondServiceListener<T, ObjectType>::ProcessAdd(T &data)
{
    Next_Service_ptr->Addtrade(data);
}



template<typename T>
BMSD2BAES_listener<T>::BMSD2BAES_listener(Service<string, OrderBook<T>> * _Next_Service_ptr) : Next_Service_ptr(_Next_Service_ptr)
{
}


template<typename T>
void BMSD2BAES_listener<T>::ProcessAdd(OrderBook<T> &data)
{


    Next_Service_ptr->ExecuteAlgo(data);

}

#endif // BOND_CONNECTORS_HPP_INCLUDED
