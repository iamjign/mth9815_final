#ifndef BONDMARKETDATASERVICE_HPP_INCLUDED
#define BONDMARKETDATASERVICE_HPP_INCLUDED


#include "marketdataservice.hpp"



template<typename T>
class BondMarketDataService : public MarketDataService<T>
{

public:

  BondMarketDataService(Connector<OrderBook<T>> * _BMDSConnector_ptr);

   OrderBook<T>& GetData(string key);

  // The callback that a Connector should invoke for any new or updated data
   void OnMessage(OrderBook<T> &data);

  // Add a listener to the Service for callbacks on add, remove, and update events
  // for data to the Service.
   void AddListener(ServiceListener<OrderBook <T>> *listener);

  // Get all listeners on the Service.
   const vector< ServiceListener<OrderBook <T>>* >& GetListeners() const;

  // Get the best bid/offer order
   const BidOffer& GetBestBidOffer(const string &productId);

  // Aggregate the order book
   const OrderBook<T>& AggregateDepth(const string &productId);

   //using class operator as comparison function
   bool Price_comp(const double &i, const double &j){return i<j;};



private:

  map<string, OrderBook <T>> MarketOrderBook;

  vector< ServiceListener<OrderBook <T>>* > BMDSlistener_vec;


  Connector<OrderBook<T>> * BMDSConnector_ptr;

};

/**********************implementation********************************************/
template<typename T>
BondMarketDataService<T>::BondMarketDataService(Connector<OrderBook<T>> * BMDSConnector_ptr)
{
    MarketOrderBook = multimap<string, OrderBook <T>>();

    BMDSlistener_vec = vector< ServiceListener<OrderBook <T>>* >();

}

template<typename T>
OrderBook<T>& BondMarketDataService<T>::GetData(string key)
{

    return MarketOrderBook[key];


}

template<typename T>
void BondMarketDataService<T>::OnMessage(OrderBook<T> &data)
{

    MarketOrderBook.insert(pair<string, OrderBook<T>>(data.GetProduct().GeProductId(), data));

}

template<typename T>
void BondMarketDataService<T>::AddListener(ServiceListener<OrderBook <T>> *listener)
{

    BMDSlistener_vec.push_back(listener);

}

template<typename T>
const vector< ServiceListener<OrderBook <T>>* >& BondMarketDataService<T>::GetListeners() const
{
    return BMDSlistener_vec;

}

template<typename T>
const BidOffer& BondMarketDataService<T>::GetBestBidOffer(const string &productId)
{
    //
    auto bid_begin_ptr = MarketOrderBook[productId].GetBidStack().begin();
    auto bid_end_ptr = MarketOrderBook[productId].GetBidStack().end();
    //
    auto offer_begin_ptr = MarketOrderBook[productId].GetOfferStack().begin();
    auto offer_end_ptr = MarketOrderBook[productId].GetOfferStack().end();

    auto leastbid = *std::min_element(bid_begin_ptr, bid_end_ptr,Price_comp(bid_begin_ptr->GetPrice(), bid_end_ptr->GetPrice());
    auto highestoffer = *std::max_element(offer_begin_ptr, offer_end_ptr,Price_comp(offer_begin_ptr->GetPrice(), offer_end_ptr->GetPrice());

    return BidOffer(leastbid, highestoffer);
}

template<typename T>
const OrderBook<T>& BondMarketDataService<T>::AggregateDepth(const string &productId)
{

    return MarketOrderBook[productId];
}




#endif // BONDMARKETDATASERVICE_HPP_INCLUDED
