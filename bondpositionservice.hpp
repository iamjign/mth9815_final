#ifndef BOND_POSITION_SERVICE_HPP
#define BOND_POSITION_SERVICE_HPP

#include "positionservice.hpp"


template <typename T>
class BondPositionService: public PositionService<Position<T>>
{
public:
    BondPositionService();

    Position<T>& GetData(string key);

    // The callback that a Connector should invoke for any new or updated data
    //void OnMessage(Position<T> &data);

    // Add a listener to the Service for callbacks on add, remove, and update events
    // for data to the Service.
    AddListener(ServiceListener<Position<T>> *listener);

    // Get all listeners on the Service.
    const vector< ServiceListener<Position<T>>* >& GetListeners() const;

    void AddTrade(const Trade<T> &trade);

private:
    map<string, Position<T>> BondPositionBook;
    vector< ServiceListener<Position<T>* > ServiceListener_vec;

};

template<typename T>
BondPositionService<T>::BondPositionService()
{
    BondPositionBook = multimap<string, Position<T>>();
    ServiceListener_vec = vector< ServiceListener<Position<T>* > ();


}

template<typename T>
Position<T>& BondPositionService<T>::GetData(string key)
{

    return BondPositionBook[key];
}

template<typename T>
void BondPositionService<T>::Addlistener(ServiceListener<Position<T>> *listener)
{
    ServiceListener_vec.push_back(listener);

}

template<typename T>
const vector< ServiceListener<Position<T>>* >& BondPositionService<T>::GetListeners() const
{
    return ServiceListener_vec;

}


template<typename T>
void BondPositionService<T>::AddTrade(const Trade<T> &trade)
{
    Position<T> temp_pos(trade.GetProduct(), trade.GetBook(), trade.GetQuantity());
    BondPositionBook.insert(pair<string, Position<T>>(trade.GetProduct().GetProductId(), temp_pos));

}





#endif
