#ifndef BONDPRICINGSERVICE_HPP_INCLUDED
#define BONDPRICINGSERVICE_HPP_INCLUDED

#include <string>
#include <vector>
#include <unordered_map>
#include "pricingservice.hpp"

template<typename T>
class BondPricingService: PricingService<T>
{

public:
    //ctor
    BondPricingService();

    Price<T>& GetData(string key);

    //T would be a vector data coming from connector
    void OnMessage(Price<T> &data);

    void AddListener(ServiceListener<Price<T>> *listener);

    const vector< ServiceListener<Price<T>>* >& GetListeners() const;

private:

    multimap<string, Price<T> > pricingbook; //This is the main ledger

    //keeps list of all Servicelistener registered to this service
    vector< ServiceListener<Price<T>>* > BondServiceListener_vec;
};


template<typename T>
BondPricingService<T>::BondPricingService()
{


    pricingbook = multimap<string, Price<T> >();


}

template<typename T>
Price<T>& BondPricingService<T>::GetData(string key)
{

    return pricingbook[key];
}

template<typename T>
void BondPricingService<T>::OnMessage(Price<T>& data)

#endif // BONDPRICINGSERVICE_HPP_INCLUDED
