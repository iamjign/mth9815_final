import numpy as np
import pandas as pd
import random


#generate price.txt and marketdata.txt, preprocessing will be done c++ wrapper function or connector class
for idx in range(0,2):
	#simulate bond prices for 2, 3, 5, 7, 10, 30yrs
	cols = ['2YR', '3YR', '5YR','7YR','10YR','30YR' ]
	#idx = pd.Index(list('name'), name='index_name')
	#data = np.random.randint(0, 32, (10, 6))
	xy = np.random.randint(32, size=(1000000, 6)).astype(str)
	z = np.random.randint(8, size=(1000000, 6)).astype(str)

	data2 = np.random.randint(99, 102, size=(1000000, 6)).astype(str)
	df_xy_term = pd.DataFrame(xy, columns=cols).apply(lambda x: x.str.zfill(2))

	df_z_term = pd.DataFrame(z, columns=cols).apply(lambda x: x.str.zfill(1))
	#df_z_term.replace('4', '+', inplace=True)
	df_dollar_term=pd.DataFrame(data2, columns=cols)
	df_bond_price = df_dollar_term+'-'+df_xy_term+df_z_term
	if idx == 0:
		df_bond_price.to_csv('/home/xubuntu/Documents/MTH9815/prices.txt')
	else:
		df_bond_price.to_csv('/home/xubuntu/Documents/MTH9815/marketdata.txt')

#Generate the trades.txt
cols2 = ['Product', 'Book', 'Quantity', 'Side']
Side_choice = ['BUY', 'SELL']

#positions between 1 and 199
q = np.random.randint(199, size=(60, 6)).astype(str)

#create the size of the trade; also used in inquiry generation
idx = np.arange(1, 61, 1)


tradebook = pd.DataFrame(columns = cols2, index =pd.Index(idx, name='TradeId'))
tradebook.Product = 'Bond'
tradebook.Book[0:10] = 'TRSY2'
tradebook.Book[10:20] = 'TRSY3'
tradebook.Book[20:30] = 'TRSY5'
tradebook.Book[30:40] = 'TRS7'
tradebook.Book[40:50] = 'TRSY10'
tradebook.Book[50:60] = 'TRSY30'
tradebook.Quantity = q
tradebook.Side = tradebook.Side.apply(lambda x:random.choice(Side_choice))
tradebook.to_csv('/home/xubuntu/Documents/MTH9815/trades.txt')



#Generate an inquiry.txt
cols3=['Product', 'Book', 'Side', 'Quantity', 'Price', 'Inquiry_State']

#price 
xy = np.random.randint(99, 101, size=(60, 6))
z = np.random.randint(257, size=(60, 6))
#ensure the minimum difference quote is within 1/256
price = xy.astype(float)+z.astype(float)/256.

Inquirybook = pd.DataFrame(columns = cols3, index=pd.Index(idx, name='InquiryId'))
Inquirybook.Product = 'Bond'
Inquirybook.Book[0:10] = 'TRSY2'
Inquirybook.Book[10:20] = 'TRSY3'
Inquirybook.Book[20:30] = 'TRSY5'
Inquirybook.Book[30:40] = 'TRS7'
Inquirybook.Book[40:50] = 'TRSY10'
Inquirybook.Book[50:60] = 'TRSY30'
Inquirybook.Side = Inquirybook.Side.apply(lambda x:random.choice(Side_choice))
Inquirybook.Quantity = q
Inquirybook.Price = price
Inquirybook.Inquiry_State = 'RECEIVED'

Inquirybook.to_csv('/home/xubuntu/Documents/MTH9815/inquiries.txt')






