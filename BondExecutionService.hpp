#ifndef BONDEXECUTIONSERVICE_HPP_INCLUDED
#define BONDEXECUTIONSERVICE_HPP_INCLUDED


#include "executionservice.hpp"



template<typename T>
class BondExecutionService : public Service<string,ExecutionOrder <T> >
{

public:

    BondExecutionService();

    ExecutionOrder<T> & GetData(string key);

    //
    void OnMessage(ExecutionOrder<T> &data);

    // Add a listener to the Service for callbacks on add, remove, and update events
    // for data to the Service.
    void AddListener(ServiceListener<ExecutionOrder<T>> *listener);

    // Get all listeners on the Service.
    const vector< ServiceListener<ExecutionOrder<T>>* >& GetListeners() const;

        // Execute an order on a market
  void ExecuteOrder(const ExecutionOrder<T>& order, Market market);

private:

    vector< ServiceListener<ExecutionOrder<T>>* > BES_listener_vec;


    map<string, ExecutionOrder<T>>  ExecutionOrderRecord;

    Connector<ExecutionOrder<T>> *BES2Market_connector;



};

template<typename T>
BondExecutionService::BondExecutionService(Connector<ExecutionOrder<T>> *_BES2Market_connector):
BES2Market_connector(_BES2Market_connector)
{
    BES_listener_vec=vector< ServiceListener<ExecutionOrder<T>>* >();
    ExecutionOrderRecord=map<string, ExecutionOrder<T>>();
}

template<typename T>
ExecutionOrder<T> & BondExecutionService::GetData(string key)
{

    return ExecutionOrderRecord[key];
}

template<typename T>
void BondExecutionService::OnMessage(ExecutionOrder<T> &data)
{
    ExcutionOrderRecord.insert(pair<string, ExecutionOrder<T>>(data.GetProduct().GetProductId(), data));
}

template<typename T>
void BondExecutionService::AddListener(ServiceListener<ExecutionOrder<T>> *listener)
{
    BES_listener_vec.push_bacK(listener);
}

template<typename T>
const vector< ServiceListener<ExecutionOrder<T>>* >& BondExecutionService::GetListeners() const
{
    return BES_listener_vec;
}

template<typename T>
void BondExecutionService::ExecuteOrder(const ExecutionOrder<T>& order, Market market)
{
    for(auto &iter:ExecutionOrderRecord)
    {
        BES2Market_connector->Publish(iter.second);

    }

}



#endif // BONDEXECUTIONSERVICE_HPP_INCLUDED
